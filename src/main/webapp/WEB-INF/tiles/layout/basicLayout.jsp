<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><tiles:getAsString name="title" /></title>
		<meta name="author" content="Markus Sairanen">
		<meta name="_csrf" content="${_csrf.token}"/>
		<!-- default header name is X-CSRF-TOKEN -->
		<meta name="_csrf_header" content="${_csrf.headerName}"/>
		<link href="/css/bootstrap.css" rel="stylesheet">
		<link href="/css/bootstrap-theme.css" rel="stylesheet">
		<link href="/css/jquery-ui.css" rel="stylesheet">
		<link href="/css/jquery-ui.theme.css" rel="stylesheet">
		<link href="/css/default.css" rel="stylesheet">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div>
					<tiles:insertAttribute name="header" />
				</div>
			</div>
			<div class="row">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
		
		<script src="/js/jquery.min.js"></script>
	    <script src="/js/jquery-ui.js"></script>
		<script src="/js/bootstrap.js"></script>

	    <script>
	    	$(function() {
	    		$("#datepicker").datepicker({
	    			dateFormat: "yy-mm-dd",
	    			firstDay: 1,
	    			altField: "#date",
	    			onSelect: function(dateText, inst) {
	    				document.dateForm.submit();
	    			}
	    		});
	    		$("#datepicker").datepicker("setDate", '<c:out value="${selectedDate}" />');
	    		
	    		$(".editFavouriteProgramLink").click(function(event) {
					event.preventDefault();
					$.getJSON("favouriteedit/" + $(this).attr("data-programId"))
						.done(function(data) {
							$('#programId').val(data.id);
							$('#programName').val(data.name);
							$('#formTitle').text("Muokkaa suosikki-ohjelmaa");
							$('#submit').val("Tallenna muutokset");
							$("#cancelEdit").empty();
							$("#cancelEdit").append('<input type="reset" value="Peruuta muutokset" id="reset" />');
							$('html, body').animate({scrollTop: 0}, "slow");
						});
					});
				$("#cancelFavouriteProgramEdit").on("click", "input", function() {
					$('#programName').val('');
					$('#programId').val('');
					$('#formTitle').text("Lis�� suosikki-ohjelma");
					$('#submit').val("Lis�� ohjelma suosikkeihin");
					$("#cancelEdit").empty();
				});
				
				$(".editChannelLink").click(function(event) {
					event.preventDefault();
					$.getJSON("channeledit/" + $(this).attr("data-channelId"))
						.done(function(data) {
							$('#channelId').val(data.id);
							$('#channelName').val(data.name);
							$('#channelLogo').val(data.logo);
							$('#channelParameter').val(data.parameter);
							$('#channelActive').prop('checked', data.active);
							$('#formTitle').text("Muokkaa kanavaa");
							$('#submit').val("Tallenna muutokset");
							$("#cancelChannelEdit").empty();
							$("#cancelChannelEdit").append('<input type="reset" value="Peruuta muutokset" id="reset" />');
							$('html, body').animate({scrollTop: 0}, "slow");
						});
					});
				$("#cancelChannelEdit").on("click", "input", function() {
					$('#channelName').val('');
					$('#channelId').val('');
					$('#channelLogo').val('');
					$('#channelParameter').val('');
					$('#channelActive').val('');
					$('#formTitle').text("Lis�� kanava");
					$('#submit').val("Lis�� kanava");
					$("#cancelChannelEdit").empty();
				});	
	    	});
	    </script>
	</body>
</html>
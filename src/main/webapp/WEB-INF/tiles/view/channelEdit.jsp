<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="/WEB-INF/tlds/custom.tld" %>

<div class="col-md-12">
	<div>
		<h3 id="formTitle">Lis�� kanava</h3>
		<form action="addChannel" method="post" accept-charset="utf-8">
			<label for="channelName">Nimi</label>
			<input type="text" name="name" id="channelName" />
			<label for="channelLogo">Logo</label>
			<input type="text" name="logo" id="channelLogo" />
			<label for="channelParameter">Parametri</label>
			<input type="text" name="parameter" id="channelParameter" />
			<label for="channelActive">Aktiivinen</label>
			<input type="checkbox" name="active" checked id="channelActive" />
			<input type="submit" id="add" value="Lis�� kanava">
			<input type="hidden" name="id" id="channelId" readonly="readonly" />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<span id="cancelChannelEdit" />
		</form>
	</div>
	<div class="channel-list">
		<ul class="list-unstyled">
			<c:forEach items="${channelList}" var="channel">
				<li class="channel">
					<div class="name">${channel.name}</div>
					<div class="name">${channel.logo}</div>
					<div class="name">${channel.parameter}</div>
					<div class="name"><c:out value="${channel.active ? 'Aktiivinen' : 'Ep�aktiivinen'}" /></div>
					<div class="actions">
						<a href="#" class="editChannelLink" data-channelId="${channel.id}">Muokkaa</a>
					</div>
					<div class="clearFloat" />
				</li>
			</c:forEach>
		</ul>
	</div>
</div>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="/WEB-INF/tlds/custom.tld" %>

<div class="col-md-4 activity_box_4">
	<h3>T�ll� hetkell� tulevat ohjelmat:</h3>
	<ul id="currentList" class="frontPagePrograms">
		<c:forEach items="${currents}" var="program">
		<li>
			<div>
				<div class="channelLogo">
					<img alt="${program.channel.name}" src="images/${program.channel.logo}" title="${program.channel.name}" />
				</div>
				<div class="programDetails">
					<div class="programName">
						${program.name}
					</div>
					<div class="programStartDateTime">
						<fmt:formatDate value="${program.startTime}" pattern="HH:mm" />
					</div>
					<div class="programDescription">
						${program.description}
					</div>
				</div>
			</div>
		</li>
		</c:forEach>
	</ul>
</div>
<div class="col-md-4 activity_box_4">
	<h3>L�ytyneet suosikkiohjelmat seuraavan viikon ajalta:</h3>
	<ul id="favouriteList" class="frontPagePrograms">
		<c:forEach items="${favourites}" var="program">
		<li>
			<div>
				<div class="channelLogo">
					<img alt="${program.channel.name}" src="images/${program.channel.logo}" title="${program.channel.name}" />
				</div>
				<div class="programDetails">
					<div class="programName">
						${program.name}
					</div>
					<div class="programStartDateTime">
						<fmt:formatDate value="${program.startTime}" pattern="dd.MM. HH:mm" />
					</div>
					<div class="programDescription">
						${program.description}
					</div>
				</div>
			</div>
		</li>
		</c:forEach>
	</ul>
</div>
<div class="col-md-4 activity_box_4">
	<h3>L�ytyneet elokuvat seuraavan viikon ajalta:</h3>
	<ul id="movieList" class="frontPagePrograms">
		<c:forEach items="${movies}" var="program">
		<li>
			<div>
				<div class="channelLogo">
					<img alt="${program.channel.name}" src="images/${program.channel.logo}" title="${program.channel.name}" />
				</div>
				<div class="programDetails">
					<div class="programName">
						${program.name}
					</div>
					<div class="programStartDateTime">
						<fmt:formatDate value="${program.startTime}" pattern="dd.MM. HH:mm" />
					</div>
					<div class="programDescription">
						${program.description}
					</div>
				</div>
			</div>
		</li>
		</c:forEach>
	</ul>
</div>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="/WEB-INF/tlds/custom.tld" %>

<div class="col-md-12">
	<div>
		<h3 id="formTitle">Lis�� suosikki-ohjelma</h3>
		<form action="addFavourite" method="post" accept-charset="utf-8">
			<label for="programName">Nimi</label>
			<input type="text" name="name" id="programName" />
			<input type="submit" id="add" value="Lis�� ohjelma suosikkeihin">
			<input type="hidden" name="id" id="programId" readonly="readonly" />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<span id="cancelFavouriteProgramEdit" />
		</form>
	</div>
	<div class="favouriteProgram-list">
		<ul class="list-unstyled">
			<c:forEach items="${favouriteList}" var="program">
				<li class="favouriteProgram">
					<div class="name">${program.name}</div>
					<div class="actions">
						<a href="#" class="editFavouriteProgramLink" data-programId="${program.id}">Muokkaa</a> |
						<a href="deleteFavourite/${program.id}">Poista</a>
					</div>
					<div class="clearFloat" />
				</li>
			</c:forEach>
		</ul>
	</div>
</div>
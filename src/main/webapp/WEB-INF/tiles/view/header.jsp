<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/home">JMS</a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
			<li class="${activePage == 'allchannels' ? 'active' : ''}"><a href="/allchannels">Kaikki kanavat</a></li>
			<li class="${activePage == 'singlechannel' ? 'active' : ''}">
				<a href="/singlechannel">Yksittäinen kanava</a>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin<strong class="caret"></strong></a>
				<ul class="dropdown-menu">
					<li class="${activePage == 'channeledit' ? 'active' : ''}"><a href="/admin/channeledit">Kanavien muokkaus</a></li>
					<li class="${activePage == 'favouriteedit' ? 'active' : ''}"><a href="/admin/favouriteedit">Suosikkien muokkaus</a></li>
				</ul>
			</li>
		</ul>

		<c:if test="${(activePage == 'allchannels') or (activePage == 'singlechannel')}">
			<c:set value="${activePage}" var="act" />
			<c:url value="${act}" var="prevDateUrl">
				<c:param name="d" value="${prevDate}" />
				<c:if test="${act == 'singlechannel'}">
					<c:param name="id" value="${activeChannelId}" />
				</c:if>
			</c:url>
			<c:url value="${act}" var="nextDateUrl">
				<c:param name="d" value="${nextDate}" />
				<c:if test="${act == 'singlechannel'}">
					<c:param name="id" value="${activeChannelId}" />
				</c:if>
			</c:url>
			<div>
				<ul class="nav navbar-nav" id="calendar">
					<li>
						<a href="${prevDateUrl}">
							<span class="glyphicon glyphicon-arrow-left"></span>
						</a>
					</li>
					<li id="calTextBox">
						<a href="#">
							<input type="text" size="10" id="datepicker">
							<span class="glyphicon glyphicon-calendar"></span>
						</a>
						<form id="dateForm" name="dateForm" action="${act}" method="post">
							<sec:csrfInput />
							<input type="hidden" name="d" id="date">
						</form>
					</li>
					<li>
						<a href="${nextDateUrl}">
							<span class="glyphicon glyphicon-arrow-right"></span>
						</a>
					</li>
				</ul>
			</div>
		</c:if>
		
		<form class="navbar-form navbar-right" role="search">
			<div class="form-group">
				<input class="form-control" type="text">
			</div>
			<button type="submit" class="btn btn-default">Search</button>
		</form>
	</div>
</nav>
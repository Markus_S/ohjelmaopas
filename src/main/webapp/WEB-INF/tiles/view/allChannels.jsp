<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="/WEB-INF/tlds/custom.tld" %>

<ul class="channels">
	<c:forEach items="${blockList}" var="channel">
	<li class="col-md-2 activity_box_2 channel">
		<header class="channel_header">
			<c:choose>
				<c:when test="${!empty channel.get(0)}">
					<c:set value="${channel.get(0).get(0).channel.name}" var="chName" />
					<c:set value="${channel.get(0).get(0).channel.logo}" var="chLogo" />
					<c:set value="${channel.get(0).get(0).channel.id}" var="chId" />
				</c:when>
				<c:when test="${!empty channel.get(1)}">
					<c:set value="${channel.get(1).get(0).channel.name}" var="chName" />
					<c:set value="${channel.get(1).get(0).channel.logo}" var="chLogo" />
					<c:set value="${channel.get(1).get(0).channel.id}" var="chId" />
				</c:when>
				<c:when test="${!empty channel.get(2)}">
					<c:set value="${channel.get(2).get(0).channel.name}" var="chName" />
					<c:set value="${channel.get(2).get(0).channel.logo}" var="chLogo" />
					<c:set value="${channel.get(2).get(0).channel.id}" var="chId" />
				</c:when>
				<c:when test="${!empty channel.get(3)}">
					<c:set value="${channel.get(3).get(0).channel.name}" var="chName" />
					<c:set value="${channel.get(3).get(0).channel.logo}" var="chLogo" />
					<c:set value="${channel.get(3).get(0).channel.id}" var="chId" />
				</c:when>
				<c:otherwise>
					???
				</c:otherwise>
			</c:choose>
			
			<c:url value="singlechannel" var="chUrl">
				<c:param name="d" value="${selectedDate}" />
				<c:param name="id" value="${chId}" />
			</c:url>
			
			<a href="${chUrl}">
				<img alt="${chName}" src="images/${chLogo}" title="${chName}" />
			</a>
		</header>
		<div class="channel_data">
			<ol class="blocks">
				<c:forEach items="${channel}" var="block">
					<li class="block"><!-- ohjelmablokki -->
						<ol class="block_programs">
							<c:forEach items="${block}" var="program">
								<li class="program">
									<div class="programStartTime">
										<fmt:formatDate value="${program.startTime}" pattern="HH:mm" />
									</div>
									<div class="programName">
										${program.name}
									</div>
								</li>
							</c:forEach>
						</ol>
					</li>
				</c:forEach>
			</ol>
		</div>
	</li>
	</c:forEach>
</ul>
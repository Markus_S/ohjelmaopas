package com.myqnapcloud.sonata.ohjelmaopas.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table
public class Program extends PersistentClass {
	@Transient
	private static final long serialVersionUID = -11626419636266823L;
	
	@ManyToOne
	@JoinColumn(name = "channelId", foreignKey = @ForeignKey(name = "channelIndex"))
	private Channel channel;
	
	@Lob
	private String description;
	
	@Column(nullable = false)
	private LocalDateTime startTime;
	
	private int length;
	
	@Column(nullable = false)
	private boolean movie;
	
	public Channel getChannel() {
		return channel;
	}
	
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}
	
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public boolean isMovie() {
		return movie;
	}
	
	public void setMovie(boolean movie) {
		this.movie = movie;
	}
	
	@Override
	public String toString() {
		return "Program [channel=" + channel + ", description=" + description + ", startTime=" + startTime + ", length="
				+ length + ", movie=" + movie + ", toString()=" + super.toString() + "]";
	}
}

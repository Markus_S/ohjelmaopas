package com.myqnapcloud.sonata.ohjelmaopas.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table
public class FavouriteProgram extends PersistentClass {

	@Transient
	private static final long serialVersionUID = 2969557652774221027L;
	
	@Override
	public String toString() {
		return "FavouriteProgram [toString()=" + super.toString() + "]";
	}
}

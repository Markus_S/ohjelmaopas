package com.myqnapcloud.sonata.ohjelmaopas.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table
public class Channel extends PersistentClass {
	@Transient
	private static final long serialVersionUID = -5805124774046506388L;
	
	@Column(nullable = false)
	private String logo;
	
	@Column(nullable = false)
	private String parameter;
	
	@Column(nullable = false)
	private boolean active;

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Channel [logo=" + logo + ", parameter=" + parameter + ", active=" + active + "]";
	}
}

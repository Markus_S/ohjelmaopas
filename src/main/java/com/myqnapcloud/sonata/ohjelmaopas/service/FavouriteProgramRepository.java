package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.repository.Repository;

import com.myqnapcloud.sonata.ohjelmaopas.domain.FavouriteProgram;

public interface FavouriteProgramRepository extends Repository<FavouriteProgram, Long> {
	FavouriteProgram save(FavouriteProgram favouriteProgram);
	Optional<FavouriteProgram> findOne(Long id);
	Stream<FavouriteProgram> findAll();	
	boolean exists(Long id);
	long count();
	void delete(Long id);
	void deleteAll();
}

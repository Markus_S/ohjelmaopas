package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;

public class Telvis_fi implements Scraper {

	private final static Logger logger = LoggerFactory.getLogger(Telvis_fi.class); 
	private final static String BASEURL = "http://www.telvis.fi/tvohjelmat/";
	private final static String PARAMETERS = "?vw=channel&sh=all";
	private final static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	private final static String charset ="iso-8859-1";
	
	@Override
	public List<Program> scrape(Channel channel, LocalDate date) throws IOException {
		List<Program> programList = new ArrayList<>();
		
		try {
			URL url = getUrl(channel, date);
			Document doc = Jsoup.parse(url.openStream(), charset, url.toString());
			
			// Adjust escape mode
			doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
			
			Elements programs = doc.select("div.program");
			
			if (programs.isEmpty()) {
				logger.info("No program information for channel " + channel.getName() + " on " + dtf.format(date));
			} else {
				for (Element program : programs) {
					Elements programInformation;

					if ((programInformation = program.select("div.chndesc")).isEmpty()) {
						// Picture in program info => get info elsewhere
						programInformation = program.select("div.chnsplitted");
					}

					programInformation.select("span.iconset").remove();
					String startTime = programInformation.select("span.time").html();
					programInformation.select("h4").select("span.time").remove();
					programInformation.select("h4").select("img").remove();
					String name = StringEscapeUtils.unescapeHtml4(programInformation.select("h4").html().replace("&apos;", "'"));
					programInformation.select("h4").remove();
					String desc = StringEscapeUtils.unescapeHtml4(programInformation.html().replace("&apos;", "'"));

					Program tmpProgram = new Program();
					tmpProgram.setName(name);
					tmpProgram.setDescription(desc);

					LocalDateTime tmpDate = prepareStartTime(date, startTime);
					tmpProgram.setStartTime(tmpDate);

					programList.add(tmpProgram);
				}
			}
		} catch (NumberFormatException|IOException e) {
			throw e;
		}
		
		return programList;
	}

	private static LocalDateTime prepareStartTime(LocalDate date, String startTime) throws NumberFormatException {
		int startTimeHours = Integer.parseInt(startTime.substring(0, 2));
		int startTimeMinutes = Integer.parseInt(startTime.substring(3));
		
		// If start time is before 5 am => change to next day
		LocalDate ld = startTimeHours < 5? date.plusDays(1) : date;
		
		LocalTime lt = LocalTime.of(startTimeHours, startTimeMinutes);
		
		return LocalDateTime.of(ld, lt);
	}

	private static URL getUrl(Channel channel, LocalDate date) throws MalformedURLException {
		URL url = null;

		url = new URL(BASEURL + PARAMETERS + "&ch=" + channel.getParameter() + "&dy=" + dtf.format(date));

		return url;
	}
}

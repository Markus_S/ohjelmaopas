package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

public final class LocalDateConverter implements Converter<String, LocalDate> {

	private final static Logger logger = LoggerFactory.getLogger(LocalDateConverter.class);
	private final DateTimeFormatter formatter;

	public LocalDateConverter(String dateFormat) {
		this.formatter = DateTimeFormatter.ofPattern(dateFormat);
	}

	@Override
	public LocalDate convert(String source) {
		if (source == null || source.isEmpty()) {
			return null;
		}

		LocalDate localdate = null;

		try {
			localdate = LocalDate.parse(source, formatter);
		} catch (DateTimeParseException e) {
			logger.error("Date parsing failed for value: " + source);
		}

		return localdate;
	}
}

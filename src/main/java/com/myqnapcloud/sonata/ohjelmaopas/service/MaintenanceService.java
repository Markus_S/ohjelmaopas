package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;

@Service
@Transactional
public class MaintenanceService {
	private final static Logger logger = LoggerFactory.getLogger(MaintenanceService.class);
	private static boolean debugMode;
	private static final ZoneId TIME_ZONE = ZoneId.of("Europe/Helsinki");
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private ScheduledFuture<?> maintenanceHandle;
	private final ChannelRepository channelRepository;
	private final ProgramRepository programRepository;
	private final ScraperService scraperService;
	private boolean running;

	@Autowired
	private Environment env;

	@Autowired
	public MaintenanceService(ChannelRepository channelRepository, ProgramRepository programRepository, ScraperService scraperService) {
		this.channelRepository = channelRepository;
		this.programRepository = programRepository;
		this.scraperService = scraperService;
	}

	@PostConstruct
	private void isDebugMode() {
		for (String profile : env.getActiveProfiles()) {
			if (profile.equals("dev")) {
				debugMode = Boolean.TRUE;
				break;
			} else {
				debugMode = Boolean.FALSE;
			}
		}
	}

	public boolean isRunning() {
		return running;
	}

	public void startMaintenance() {
		final long repeatInterval;
		final long initialDelay;

		final Runnable maintenanceRunnable = () -> {
			checkUpdates();
		};

		if (debugMode) {
			repeatInterval = 25; //Seconds
			initialDelay = 0;
		} else {
			LocalDateTime now = LocalDateTime.now(TIME_ZONE);
			repeatInterval = TimeUnit.DAYS.toSeconds(1);

			//Next day 5AM
			LocalDateTime startDateTime = now.truncatedTo(ChronoUnit.DAYS).plusDays(1).plusHours(5);

			initialDelay = now.until(startDateTime, ChronoUnit.SECONDS);
		}

		maintenanceHandle = scheduler.scheduleAtFixedRate(maintenanceRunnable, initialDelay, repeatInterval, TimeUnit.SECONDS);

		logger.info("Maintenance started!");
		running = true;
	}

	public void stopMaintenance() {
		maintenanceHandle.cancel(true);
		logger.info("Maintenance stopped!");
		running = false;
	}

	private void checkUpdates() {
		logger.info("Checking updates...");

		try (Stream<Channel> channels = channelRepository.findAll()) {
			LocalDateTime now = LocalDateTime.now(TIME_ZONE);

			//Set the end date to 2 or 8 days depending if we are in debug-mode
			LocalDate scrapingEndDate = now.toLocalDate().plusDays(debugMode ? 2 : 8);

			Program defaultProgram = new Program();
			defaultProgram.setStartTime(now.minusDays(7));

			channels.forEach(channel ->
					{
						Program lastProgramFromDatabase = programRepository.findTopByChannelIsOrderByStartTimeDesc(channel).orElse(defaultProgram);
						LocalDate lastDateFromDatabase = lastProgramFromDatabase.getStartTime().truncatedTo(ChronoUnit.DAYS).toLocalDate();
						logger.info("Last program date from datebase to channel " + channel.getName() + " is: " + lastDateFromDatabase);

						if (scrapingEndDate.isAfter(lastDateFromDatabase)) {
							for (LocalDate ld = lastDateFromDatabase; ld.isBefore(scrapingEndDate); ld = ld.plusDays(1)) {
								logger.info("Scraping programs for date: " + ld + " and for channel: " + channel.getName());
								scraperService.scrapePrograms(channel, ld);
							}
						} else {
							logger.info("No need to scrape for channel: " + channel.getName() + " last date from database: " + lastDateFromDatabase);
						}
					}
				);
		}
	}
}

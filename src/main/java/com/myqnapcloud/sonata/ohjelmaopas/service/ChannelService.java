package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;

@Service
@Transactional
public class ChannelService {
	private final ChannelRepository channelRepository;
	private final static Logger logger = LoggerFactory.getLogger(ChannelService.class);

	@Autowired
	public ChannelService(ChannelRepository channelRepository) {
		this.channelRepository = channelRepository;
	}

	public Channel save(Channel channel) {
		return channelRepository.save(channel);
	}

	public Optional<Channel> findOne(Long id) {
		return channelRepository.findOne(id);
	}

	public Stream<Channel> findAll() {
		return channelRepository.findAll();
	}

	public long count() {
		return channelRepository.count();
	}

	public boolean exists(Long id) {
		return channelRepository.exists(id);
	}

	public void delete(Long id) {
		channelRepository.delete(id);
	}

	public void deleteAll() {
		channelRepository.deleteAll();
	}

	public Optional<List<Channel>> findByActiveTrue() {
		return channelRepository.findByActiveTrue();
	}

	/**
	 * Insert channels from <i>channels.properties</i>-file to database.
	 */
	public void bootstrap() {
		try (InputStream is = this.getClass().getResourceAsStream("/channels.properties")) {
			Properties prop = new Properties();
			prop.load(is);

			List<String> keys = prop.stringPropertyNames().stream().collect(Collectors.toList());
			Collections.sort(keys);

			for (String key : keys) {
				String[] splittedChannel = ((String)prop.get(key)).split(";");

				Channel channel = new Channel();
				channel.setActive(true);
				channel.setName(splittedChannel[0]);
				channel.setLogo(splittedChannel[1]);
				channel.setParameter(splittedChannel[2]);

				channelRepository.save(channel);
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
}

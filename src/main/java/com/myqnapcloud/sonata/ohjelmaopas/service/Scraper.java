package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.time.LocalDate;
import java.util.List;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;

public interface Scraper {
	List<Program> scrape(Channel channel, LocalDate date) throws Exception;
}

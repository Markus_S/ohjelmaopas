package com.myqnapcloud.sonata.ohjelmaopas.service;

public enum Constants {
	DAY_STARTS_AT (5), //AM
	DAYS_IN_WEEK (7);
	
	private final int value;
	
	private Constants(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}

package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;

@Service
@Transactional
public class ProgramService {
	private final ProgramRepository programRepository;
	private final ChannelRepository channelRepository;
	private static final ZoneId TIME_ZONE = ZoneId.of("Europe/Helsinki");
	private List<LocalDateTime> timeBlocks = new ArrayList<>();

	@Autowired
	public ProgramService(ProgramRepository programRepository, ChannelRepository channelRepository) {
		this.programRepository = programRepository;
		this.channelRepository = channelRepository;
	}

	public Program save(Program program) {
		return this.programRepository.save(program);
	}

	public Stream<Program> findAll() {
		return this.programRepository.findAll();
	}

	public Optional<Program> findOne(Long id) {
		return this.programRepository.findOne(id);
	}

	public Stream<Program> findByProgramStartTimeBetween(Date startTime, Date endTime) {
		return programRepository.findByStartTimeBetween(startTime, endTime);
	}

	public Stream<Program> findByMovieTrue() {
		return programRepository.findByMovieTrue();
	}

	public Stream<Program> findMoviesBetween(LocalDateTime startTime, LocalDateTime endTime) {
		return programRepository.findByMovieTrueAndStartTimeBetweenOrderByStartTimeAsc(startTime, endTime);
	}

	public Stream<Program> findThisWeeksMovies(){
		//Set startTime to 5AM
		LocalDateTime startTime = LocalDateTime.now(TIME_ZONE).truncatedTo(ChronoUnit.DAYS).withHour(Constants.DAY_STARTS_AT.getValue());
		LocalDateTime endTime = startTime.plusDays(Constants.DAYS_IN_WEEK.getValue());

		return findMoviesBetween(startTime, endTime);
	}

	public Stream<Program> findCurrentPrograms() {
		LocalDateTime now = LocalDateTime.now(TIME_ZONE);
		List<Program> programList = new ArrayList<>();

		try (Stream<Channel> channels = channelRepository.findAll()) {
			channels.forEach(
					channel -> programRepository
								.findTopByStartTimeBeforeAndChannelIsOrderByStartTimeDesc(now, channel)
								.ifPresent(program -> programList.add(program))
							);
		}

		return programList.stream();
	}

	public List<List<List<Program>>> findProgramsForAllChannels(LocalDate date) {
		List<List<List<Program>>> blockList = new ArrayList<>();
		formTimeBlocks(date);

		channelRepository.findAll()
			.forEach(ch -> getPrograms(blockList, ch));

		return blockList;
	}

	public List<List<List<Program>>> findProgramsForSingleChannel(LocalDate date, Channel channel) {
		List<List<List<Program>>> blockList = new ArrayList<>();
		formTimeBlocks(date);

		channelRepository.findOne(channel.getId())
			.ifPresent(ch -> getPrograms(blockList, ch));

		return blockList;
	}

	private void getPrograms(List<List<List<Program>>> blockList, Channel channel) {
		List<List<Program>> programsList = new ArrayList<>();

		for (int i = 1; i < timeBlocks.size(); i++) {
			programsList.add(
				programRepository
					.findByStartTimeBetweenAndChannelIsOrderByStartTimeAsc(
							//Prevent duplicates by reducing ending time by one minute
							timeBlocks.get(i - 1), timeBlocks.get(i).minusMinutes(1), channel)
					.collect(Collectors.toList())
			);
		}

		blockList.add(programsList);
	}

	private void formTimeBlocks(LocalDate date) {
		timeBlocks.clear();

		for (int i = 1; i < 6; i++) {
			timeBlocks.add(LocalDateTime.of(date, LocalTime.MIDNIGHT).plusHours(i * 6));
		}
	}

	public boolean exists(Long id) {
		return channelRepository.exists(id);
	}

	public long count() {
		return channelRepository.count();
	}
}

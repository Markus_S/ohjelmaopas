package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;

@Service
public class ScraperService {

	private final static Logger logger = LoggerFactory.getLogger(ScraperService.class);
	private static final ZoneId TIME_ZONE = ZoneId.of("Europe/Helsinki");

	private ProgramService programService;
	private ChannelService channelService;

	private final Scraper scraper;

	@Autowired
	public ScraperService(ProgramService programService, ChannelService channelService) {
		this.scraper = new Telvis_fi();
		this.programService = programService;
		this.channelService = channelService;
	}

	public void scrapePrograms(Channel channel, LocalDate day) {
		try {
			List<Program> programs = scraper.scrape(channel, day);

			getProgramLengths(programs);

			for (Program program : programs) {
				program.setChannel(channel);
				program.setMovie(isProgramMovie(program));
				programService.save(program);
				logger.info("Scraped program: " + program);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	private void getProgramLengths(List<Program> programs) {
		ListIterator<Program> iterator = programs.listIterator(programs.size());
		Program nextProgram = null;
		while(iterator.hasPrevious()) {
			//Save the last program from list
			if (!iterator.hasNext()) {
				nextProgram = iterator.previous();
			} else {
				Program currentProgram = iterator.previous();
				int length = calculateLength(currentProgram, nextProgram);
				currentProgram.setLength(length);
				nextProgram = currentProgram;
			}
		}
	}

	private static int calculateLength(Program currentProgram, Program nextProgram) {
		return (int) ChronoUnit.MINUTES.between(currentProgram.getStartTime(), nextProgram.getStartTime());
	}

	private boolean isProgramMovie(Program program) {
		boolean movie = false;
		String name = program.getName();
		String description = program.getDescription();
		int length = program.getLength();

		if (isMovie(length, name, description) && !isSerie(description) && !falsePositive(name)) {
			movie = true;
		}

		return movie;
	}

	private boolean isMovie(int length, String name, String description) {
		return (name.toLowerCase().startsWith("dokumenttielokuva") ||
				name.toLowerCase().startsWith("elokuva") ||
				name.toLowerCase().startsWith("leffa")
				)
				||
				(length > 80 &&
					(description.toLowerCase().contains("komedia") ||
					description.toLowerCase().contains("trilleri") ||
					description.toLowerCase().contains("draama") ||
					description.toLowerCase().contains("elokuva") ||
					description.toLowerCase().contains("ohjaaja") ||
					description.toLowerCase().contains("ohjaus") ||
					description.toLowerCase().contains("pääosissa") ||
					description.toLowerCase().contains("o:") ||
					description.toLowerCase().contains("n:") ||
					description.toLowerCase().contains("p:") ||
					description.toLowerCase().contains("esittää")
				)
			);
	}

	private boolean isSerie(String description) {
		return 	description.toLowerCase().contains(". kausi") ||
				description.toLowerCase().contains("realitysarja") ||
				description.toLowerCase().contains("asiaohjelma") ||
				description.toLowerCase().contains("-sarja") ||
				description.toLowerCase().contains("dokumenttisarja") ||
				description.toLowerCase().contains("sarja alkaa") ||
				description.toLowerCase().contains("juonta") ||
				description.toLowerCase().contains("draamasarja") ||
				description.toLowerCase().contains("kommentaattori") ||
				description.toLowerCase().contains("liturgia") ||
				description.toLowerCase().contains("ortodoksi") ||
				description.toLowerCase().contains("elokuvamusiik") ||
				description.toLowerCase().contains("uusi sarja") ||
				description.toLowerCase().matches(".*kausi\\s+\\d+,.*") || 		// kausi ?,
				description.toLowerCase().matches(".*kausi\\s+\\d+\\..*") ||	// kausi ?.
				description.toLowerCase().matches(".*\\d+/\\d+\\..*") || 		// ?/?.
				description.toLowerCase().contains("sketsisarja");
	}

	private boolean falsePositive(String name) {
		return name.equals("Huomenta Suomi");
	}

	public void bootstrap() {
		Supplier<Stream<Channel>> streamSupplier = () -> channelService.findAll();

		LocalDate today = LocalDate.now(TIME_ZONE);
//		for (int i = 0; i <= 7; i++) {
		for (int i = 0; i < 1; i++) {
			LocalDate tmpDate = today.plusDays(i);
			streamSupplier.get().forEach((channel) -> scrapePrograms(channel, tmpDate));
		}
	}
}

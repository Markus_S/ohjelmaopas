package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.repository.Repository;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;

public interface ChannelRepository extends Repository<Channel, Long> {
	Channel save(Channel channel);
	Optional<Channel> findOne(Long id);
	Stream<Channel> findAll();
	boolean exists(Long id);
	long count();
	void delete(Long id);
	void deleteAll();
	Optional<List<Channel>> findByActiveTrue();
}

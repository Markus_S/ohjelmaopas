package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.repository.Repository;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;

public interface ProgramRepository extends Repository<Program, Long> {
	Program save(Program program);
	Optional<Program> findOne(Long id);
	Stream<Program> findAll();
	Stream<Program> findByStartTimeBetween(Date startTime, Date endTime);

	Stream<Program> findByStartTimeBetweenAndChannelIsOrderByStartTimeAsc(LocalDateTime startTime, LocalDateTime endTime, Channel channel);

	Stream<Program> findByMovieTrue();
	Stream<Program> findByMovieTrueAndStartTimeBetweenOrderByStartTimeAsc(LocalDateTime startTime, LocalDateTime endTime);

	//Currently showing programs
	Optional<Program> findTopByStartTimeBeforeAndChannelIsOrderByStartTimeDesc(LocalDateTime startTime, Channel channel);
	//Newest program in database for this channel
	Optional<Program> findTopByChannelIsOrderByStartTimeDesc(Channel channel);

	Stream<Program> findByNameLikeAndStartTimeBetweenOrderByStartTimeAsc(String name, LocalDateTime startTime, LocalDateTime endTime);

	boolean exists(Long id);
	long count();
//	void delete(Long id);
//	void deleteAll();
}

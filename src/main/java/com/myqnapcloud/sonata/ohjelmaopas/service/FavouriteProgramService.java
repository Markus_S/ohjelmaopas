package com.myqnapcloud.sonata.ohjelmaopas.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myqnapcloud.sonata.ohjelmaopas.domain.FavouriteProgram;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;

@Service
@Transactional
public class FavouriteProgramService {
	private final FavouriteProgramRepository favouriteProgramRepository;
	private final ProgramRepository programRepository;
	private final static Logger logger = LoggerFactory.getLogger(FavouriteProgramService.class);
	private static final ZoneId TIME_ZONE = ZoneId.of("Europe/Helsinki");

	@Autowired
	public FavouriteProgramService(FavouriteProgramRepository favouriteProgramRepository, ProgramRepository programRepository) {
		this.favouriteProgramRepository = favouriteProgramRepository;
		this.programRepository = programRepository;
	}

	public FavouriteProgram save(FavouriteProgram favouriteProgram) {
		return favouriteProgramRepository.save(favouriteProgram);
	}

	public Optional<FavouriteProgram> findOne(Long id) {
		return favouriteProgramRepository.findOne(id);
	}

	public List<FavouriteProgram> findAll() {
		return favouriteProgramRepository.findAll().collect(Collectors.toList());
	}

	public long count() {
		return favouriteProgramRepository.count();
	}

	public boolean exists(Long id) {
		return favouriteProgramRepository.exists(id);
	}

	public void delete(Long id) {
		favouriteProgramRepository.delete(id);
	}

	public void deleteAll() {
		favouriteProgramRepository.deleteAll();
	}

	public void bootstrap() {
		String[] names = {"Uusi päivä", "Kuningaskuluttaja"};

		for (String name : names) {
			FavouriteProgram favouriteProgram = new FavouriteProgram();
			favouriteProgram.setName(name);

			favouriteProgramRepository.save(favouriteProgram);
			logger.info("Favourite program: " + name + " added.");
		}
	}

	public Stream<Program> findThisWeeksFavouritePrograms () {
		LocalDateTime startTime = LocalDateTime.now(TIME_ZONE).truncatedTo(ChronoUnit.DAYS).withHour(Constants.DAY_STARTS_AT.getValue());
		LocalDateTime endTime = startTime.plusDays(Constants.DAYS_IN_WEEK.getValue());

		return findFavouriteProgramsBetween(startTime, endTime);
	}

	public Stream<Program> findFavouriteProgramsBetween(LocalDateTime startTime, LocalDateTime endTime) {
		List<Program> programList = new ArrayList<>();

		try (Stream<FavouriteProgram> favPrograms = favouriteProgramRepository.findAll()) {
			favPrograms.forEach(
					favProgram -> programRepository
							.findByNameLikeAndStartTimeBetweenOrderByStartTimeAsc(favProgram.getName(), startTime, endTime)
							.collect(Collectors.toList())
							.forEach(
								program ->	programList.add(program)
							)
					);
		}

		return programList.stream();
	}
}

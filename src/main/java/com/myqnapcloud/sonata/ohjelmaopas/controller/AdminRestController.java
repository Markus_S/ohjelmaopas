package com.myqnapcloud.sonata.ohjelmaopas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.FavouriteProgram;
import com.myqnapcloud.sonata.ohjelmaopas.service.ChannelService;
import com.myqnapcloud.sonata.ohjelmaopas.service.FavouriteProgramService;

@RestController
@RequestMapping(value = "/admin")
public class AdminRestController {
	private FavouriteProgramService favouriteProgramService;
	private ChannelService channelService;

	@Autowired
	public AdminRestController(FavouriteProgramService favouriteProgramService, ChannelService channelService) {
		this.favouriteProgramService = favouriteProgramService;
		this.channelService = channelService;
	}

	@RequestMapping(value = "/favouriteedit/{id}", method = RequestMethod.GET, produces = "application/json")
	public FavouriteProgram editFavourites(@PathVariable Long id) {
		FavouriteProgram favouriteProgram = favouriteProgramService.findOne(id).get();

		return favouriteProgram;
	}

	@RequestMapping(value = "/channeledit/{id}", method = RequestMethod.GET, produces = "application/json")
	public Channel editChannel(@PathVariable Long id) {
		Channel channel = channelService.findOne(id).get();

		return channel;
	}
}

package com.myqnapcloud.sonata.ohjelmaopas.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.Program;
import com.myqnapcloud.sonata.ohjelmaopas.service.ChannelService;
import com.myqnapcloud.sonata.ohjelmaopas.service.FavouriteProgramService;
import com.myqnapcloud.sonata.ohjelmaopas.service.ProgramService;

@Controller
public class MainController {

	private ChannelService channelService;
	private ProgramService programService;
	private FavouriteProgramService favouriteProgramService;
	private static final ZoneId TIME_ZONE = ZoneId.of("Europe/Helsinki");

	private final static Logger logger = LoggerFactory.getLogger(MainController.class);

	@Autowired
	public MainController(ChannelService channelService, ProgramService programService,
			FavouriteProgramService favouriteProgramService) {
		this.channelService = channelService;
		this.programService = programService;
		this.favouriteProgramService = favouriteProgramService;
	}

	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
	public ModelAndView home(HttpSession session) {
		List<Program> movies = new ArrayList<>();
		List<Program> currents = new ArrayList<>();
		List<Program> favourites = new ArrayList<>();

		try (Stream<Program> movieStream = programService.findThisWeeksMovies()){
			movies = movieStream.collect(Collectors.toList());
		}

		try (Stream<Program> currentStream = programService.findCurrentPrograms()){
			currents = currentStream.collect(Collectors.toList());
		}

		try (Stream<Program> favouriteStream = favouriteProgramService.findThisWeeksFavouritePrograms()){
			favourites = favouriteStream.collect(Collectors.toList());
		}

		ModelAndView mav = new ModelAndView("site.frontpage");
		mav.addObject("movies", movies);
		mav.addObject("currents", currents);
		mav.addObject("favourites", favourites);

		session.setAttribute("activePage", "main");
		prepareDate(null, session); //Set date to current date or date from session

		return mav;
	}

	@Cacheable("programs")
	@RequestMapping(value = "allchannels")
	public ModelAndView allChannels(@RequestParam(name = "d", required = false) LocalDate date, HttpSession session) {
		date = prepareDate(date, session);

		List<List<List<Program>>> blockList = programService.findProgramsForAllChannels(date);

		ModelAndView mav = new ModelAndView("site.allChannels");
		mav.addObject("blockList", blockList);

		session.setAttribute("activePage", "allchannels");

		return mav;
	}

	@Cacheable("programs")
	@RequestMapping(value = "singlechannel")
	public ModelAndView singleChannel(
			@RequestParam(name = "d", required = false) LocalDate date,
			@RequestParam(name = "id", defaultValue = "0") Long id,
			HttpSession session) {
		date = prepareDate(date, session);

		Channel defaultChannel = new Channel();
		defaultChannel.setId(0L);

		Channel channel = channelService.findOne(id)
				.orElse(
						channelService.findAll().findFirst()
							.orElse(defaultChannel)
				);
		List<List<List<Program>>> blockList = programService.findProgramsForSingleChannel(date, channel);

		ModelAndView mav = new ModelAndView("site.singleChannel");
		mav.addObject("blockList", blockList);

		session.setAttribute("activePage", "singlechannel");
		session.setAttribute("activeChannelId", channel.getId());

		return mav;
	}

	@RequestMapping(value = "search")
	public ModelAndView search(@RequestParam(value = "searchTerm", required = false) final String searchTerm) {

		return null;
	}

	/**
	 * Checks if given date is valid and if not, try to get date from session
	 * and if it do not succeed, return current date.
	 * @param date Date to check
	 * @param session Current session
	 * @return prepared date
	 */
	private LocalDate prepareDate(LocalDate date, HttpSession session) {
		if (date == null) {
			LocalDate sessionDate = (LocalDate) session.getAttribute("selectedDate");

			if (sessionDate == null) {
				date = LocalDate.now(TIME_ZONE);
			} else {
				date = sessionDate;
			}
		}

		logger.info("Date prepared: " + date);


		session.setAttribute("selectedDate", date);
		session.setAttribute("prevDate", date.minusDays(1));
		session.setAttribute("nextDate", date.plusDays(1));
		return date;
	}
}

package com.myqnapcloud.sonata.ohjelmaopas.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.myqnapcloud.sonata.ohjelmaopas.domain.Channel;
import com.myqnapcloud.sonata.ohjelmaopas.domain.FavouriteProgram;
import com.myqnapcloud.sonata.ohjelmaopas.service.ChannelService;
import com.myqnapcloud.sonata.ohjelmaopas.service.FavouriteProgramService;
import com.myqnapcloud.sonata.ohjelmaopas.service.MaintenanceService;
import com.myqnapcloud.sonata.ohjelmaopas.service.ScraperService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

	private ChannelService channelService;
	private ScraperService scraperService;
	private FavouriteProgramService favouriteProgramService;
	private MaintenanceService maintenanceService;

	private final static Logger logger = LoggerFactory.getLogger(AdminController.class);

	@Autowired
	public AdminController(ChannelService channelService, ScraperService scraperService,
			FavouriteProgramService favouriteProgramService, MaintenanceService maintenanceService) {
		this.channelService = channelService;
		this.scraperService = scraperService;
		this.favouriteProgramService = favouriteProgramService;
		this.maintenanceService = maintenanceService;
	}

	@RequestMapping(value = "/boot", method = RequestMethod.GET)
	public ModelAndView bootstrap() {
		if (channelService.count() == 0) {
			//Insert channels to database
			this.channelService.bootstrap();

			//Scrape some programs to start
			//TODO Should this use integer-parameter to define days to scrape?
			this.scraperService.bootstrap();

			//Insert few favorite program names to database
			this.favouriteProgramService.bootstrap();
		} else {
			logger.info("Kanavat olivat jo olemassa!");
		}

		//TODO Do we need ScraperService.bootstrap() really?
		if (!maintenanceService.isRunning()) {
			this.maintenanceService.startMaintenance();
		}

		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/channeledit")
	public ModelAndView editChannels(HttpSession session) {
		List<Channel> channelList = channelService.findAll().collect(Collectors.toList()); //TODO: Miksi tämä palauttaa Streamin?

		ModelAndView mav = new ModelAndView("site.channelEdit");
		mav.addObject("channelList", channelList);

		session.setAttribute("activePage", "channeledit");

		return mav;
	}

	@RequestMapping(value = "/addChannel", method = RequestMethod.POST)
	public ModelAndView saveChannel(Channel channel) {

		logger.info("Saving " + channel);

		channelService.save(channel);

		return new ModelAndView("redirect:/admin/channeledit");
	}

	@RequestMapping(value = "/favouriteedit")
	public ModelAndView editFavourites(HttpSession session) {
		List<FavouriteProgram> favouriteProgramList = favouriteProgramService.findAll();

		ModelAndView mav = new ModelAndView("site.favouriteEdit");
		mav.addObject("favouriteList", favouriteProgramList);

		session.setAttribute("activePage", "favouriteedit");

		return mav;
	}

	@RequestMapping(value = "/addFavourite", method = RequestMethod.POST)
	public ModelAndView saveFavourite(FavouriteProgram favProg) {

		logger.info("Saving " + favProg);

		favouriteProgramService.save(favProg);

		return new ModelAndView("redirect:/admin/favouriteedit");
	}

	@RequestMapping(value = "/deleteFavourite/{programId}", method = RequestMethod.GET)
	public ModelAndView deleteFavourite(@PathVariable Long programId) {

		favouriteProgramService.delete(programId);

		return new ModelAndView("redirect:/admin/favouriteedit");
	}

	@RequestMapping(value = "/maintenance", method = RequestMethod.GET)
	public ModelAndView maintenence(@RequestParam(name = "run", required = false) boolean execute) {
		if (execute && !maintenanceService.isRunning()) {
			maintenanceService.startMaintenance();
		} else if (!execute && maintenanceService.isRunning()) {
			maintenanceService.stopMaintenance();
		}

		logger.info("Maintenance running: " + maintenanceService.isRunning());

		return new ModelAndView("redirect:/");
	}
}

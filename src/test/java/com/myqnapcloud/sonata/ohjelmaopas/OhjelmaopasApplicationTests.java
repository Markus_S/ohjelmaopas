package com.myqnapcloud.sonata.ohjelmaopas;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import com.myqnapcloud.sonata.ohjelmaopas.OhjelmaopasApplication;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = OhjelmaopasApplication.class)
@WebAppConfiguration
public class OhjelmaopasApplicationTests {

	@Test
	public void contextLoads() {
	}

}
